#!/bin/bash
# Cleaning for Lesson 02. Remove temporary users, files and dirs
#      -c local \
ansible-playbook cleanup_lesson06.yaml \
       -i inventories/test_servers/hosts.yaml \
       "$@"
