#!/bin/bash

# 2 different passwords test
ansible-playbook wordpress.yaml \
	--vault-id .v_pass1.txt \
	--vault-id .v_pass2.txt \
       -i inventory/my_servers/hosts \
       "$@"
