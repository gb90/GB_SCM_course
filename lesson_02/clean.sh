#!/bin/bash
# Cleaning for Lesson 02. Remove temporary users, files and dirs
ansible-playbook cleanup_lesson02.yaml \
       -i inventories/test_servers/hosts \
#      -c local \
       "$@"
